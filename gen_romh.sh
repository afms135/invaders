#!/bin/sh
cat invaders.h invaders.g invaders.f invaders.e > ROM

if [ $? -eq 0 ]
then
	xxd -i -c 16 ROM > ROM.h
	rm ROM
	exit 0
else
	rm ROM
	exit 1
fi

