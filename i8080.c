#include "i8080.h"

/*
// Cycle table
*/
static int cycles[256] = { //Conditional RET and CALL stored as not taken
	4, 10,  7,  5,  5,  5,  7,  4,  4, 10,  7,  5,  5,  5,  7,  4,
	4, 10,  7,  5,  5,  5,  7,  4,  4, 10,  7,  5,  5,  5,  7,  4,
	4, 10, 16,  5,  5,  5,  7,  4,  4, 10, 16,  5,  5,  5,  7,  4,
	4, 10, 13,  5, 10, 10, 10,  4,  4, 10, 13,  5,  5,  5,  7,  4,
	5,  5,  5,  5,  5,  5,  7,  5,  5,  5,  5,  5,  5,  5,  7,  5,
	5,  5,  5,  5,  5,  5,  7,  5,  5,  5,  5,  5,  5,  5,  7,  5,
	5,  5,  5,  5,  5,  5,  7,  5,  5,  5,  5,  5,  5,  5,  7,  5,
	7,  7,  7,  7,  7,  7,  7,  7,  5,  5,  5,  5,  5,  5,  7,  5,
	4,  4,  4,  4,  4,  4,  7,  4,  4,  4,  4,  4,  4,  4,  7,  4,
	4,  4,  4,  4,  4,  4,  7,  4,  4,  4,  4,  4,  4,  4,  7,  4,
	4,  4,  4,  4,  4,  4,  7,  4,  4,  4,  4,  4,  4,  4,  7,  4,
	4,  4,  4,  4,  4,  4,  7,  4,  4,  4,  4,  4,  4  ,4,  7,  4,
	5, 10, 10, 10, 11, 11,  7, 11,  5, 10, 10, 10, 11, 17,  7, 11,
	5, 10, 10, 10, 11, 11,  7, 11,  5, 10, 10, 10, 11, 17,  7, 11,
	5, 10, 10, 18, 11, 11,  7, 11,  5,  5, 10,  5, 11, 17,  7, 11,
	5, 10, 10,  4, 11, 11,  7, 11,  5,  5, 10,  4, 11, 17,  7, 11,
};

/*
// Flag register bit definitions
*/
enum flags
{
	C = 0,
	P = 2,
	AC = 4,
	Z = 6,
	S = 7
};
#define SET_FLAG(f, v) {if(v){s->F |= (1<<f);}else{s->F &= ~(1<<f);}}
#define GET_FLAG(f) (!!(s->F & (1 << f)))

/*
// Helper functions
*/
//Reads a 16-bit value from the address addr
static uint16_t m_rd16(i8080_state *s, uint16_t addr)
{
	uint8_t low = s->m_rd(s, addr);
	uint8_t high = s->m_rd(s, addr + 1);
	return (high << 8) | low;
}

//Writes a 16-bit value v to the address addr
static void m_wr16(i8080_state *s, uint16_t addr, uint16_t v)
{
	s->m_wr(s, addr, v & 0xFF);
	s->m_wr(s, addr + 1, v >> 8);
}

//Returns an instruction 8-bit immediate argument
static uint8_t im8(i8080_state *s)
{
	return s->m_rd(s, s->PC++);
}

//Returns an instruction 16-bit immediate argument
static uint16_t im16(i8080_state *s)
{
	uint8_t low = s->m_rd(s, s->PC++);
	uint8_t high = s->m_rd(s, s->PC++);
	return (high << 8) | low;
}

//Reads from a register or memory selected by the 3-bit argument sel
static uint8_t reg8rd(i8080_state *s, uint8_t sel)
{
	switch (sel & 0x7) //3-bit
	{
	case 0: return s->B;
	case 1: return s->C;
	case 2: return s->D;
	case 3: return s->E;
	case 4: return s->H;
	case 5: return s->L;
	case 6: return s->m_rd(s, s->HL);
	case 7: return s->A;
	}
	return 0; //Should not occur
}

//Writes to a register or memory selected by the 3-bit argument sel
static void reg8wr(i8080_state *s, uint8_t sel, uint8_t val)
{
	switch (sel & 0x7) //3-bit
	{
	case 0: s->B = val; break;
	case 1: s->C = val; break;
	case 2: s->D = val; break;
	case 3: s->E = val; break;
	case 4: s->H = val; break;
	case 5: s->L = val; break;
	case 6: s->m_wr(s, s->HL, val); break;
	case 7: s->A = val; break;
	}
}

//Reads from a register pair selected by the 2-bit argument sel
static uint16_t reg16rd(i8080_state *s, uint8_t sel)
{
	switch (sel & 0x3) //2-bit
	{
	case 0: return s->BC;
	case 1: return s->DE;
	case 2: return s->HL;
	case 3: return s->SP;
	}
	return 0; //Should not occur
}

//Writes to a register pair selected by the 2-bit argument sel
static void reg16wr(i8080_state *s, uint8_t sel, uint16_t val)
{
	switch (sel & 0x3) //2-bit
	{
	case 0: s->BC = val; break;
	case 1: s->DE = val; break;
	case 2: s->HL = val; break;
	case 3: s->SP = val; break;
	}
}

//Push a 16-bit value onto the stack
static void stk_push(i8080_state *s, uint16_t v)
{
	s->SP -= 2;
	m_wr16(s, s->SP, v);
}

//Pop a 16-bit value from the stack
static uint16_t stk_pop(i8080_state *s)
{
	s->SP += 2;
	return m_rd16(s, s->SP - 2);
}

//Returns if the 3-bit condition given by sel is currently true
static int condition(i8080_state *s, uint8_t sel)
{
	switch (sel & 0x7) //3-bit
	{
	case 0: return !GET_FLAG(Z); //NZ
	case 1: return GET_FLAG(Z);  //Z
	case 2: return !GET_FLAG(C); //NC
	case 3: return GET_FLAG(C);  //C
	case 4: return !GET_FLAG(P); //PO
	case 5: return GET_FLAG(P);  //PE
	case 6: return !GET_FLAG(S); //P
	case 7: return GET_FLAG(S);  //M
	}
	return 0; //Should not occur
}

//Set or reset the zero, sign or parity flags using the value val
static void setflags(i8080_state *s, uint8_t val)
{
	SET_FLAG(Z, val == 0); //Zero
	SET_FLAG(S, val & 0x80); //Sign

	//Calculate parity
	int sum = 0;
	for (int i = 0; i < 8; i++)
		sum += (val & (1 << i)) >> i;
	SET_FLAG(P, !(sum & 0x1)); //Parity
}

//Performs one of 8 ALU operations operating on A using the argument val
static void alu(i8080_state *s, uint8_t op, uint8_t val)
{
	uint8_t carry = GET_FLAG(C);

	switch (op & 0x7) //3-bit
	{
	case 0: //Add
		SET_FLAG(AC, ((s->A & 0xF) + (val & 0xF)) > 0xF);
		SET_FLAG(C, (s->A + val) > 0xFF)
		s->A += val;
		setflags(s, s->A);
		break;
	case 1: //Add with carry
		SET_FLAG(AC, ((s->A & 0xF) + (val & 0xF) + carry) > 0xF);
		SET_FLAG(C, (s->A + val + carry) > 0xFF);
		s->A += val + carry;
		setflags(s, s->A);
		break;
	case 2: //Subtract
		SET_FLAG(AC, (s->A & 0xF) >= (val & 0xF));
		SET_FLAG(C, val > s->A);
		s->A -= val;
		setflags(s, s->A);
		break;
	case 3: //Subtract with carry
		SET_FLAG(AC, (s->A & 0xF) >= ((val & 0xF) + carry));
		SET_FLAG(C, (val + carry) > s->A);
		s->A -= val + carry;
		setflags(s, s->A);
		break;
	case 4: //Bitwise AND
		//CPU oddity: set AC if bit 4 is set in either operand
		SET_FLAG(AC, (s->A & 0x08) | (val & 0x08));
		s->A &= val;
		SET_FLAG(C, 0);
		setflags(s, s->A);
		break;
	case 5: //Bitwise XOR
		s->A ^= val;
		SET_FLAG(AC, 0);
		SET_FLAG(C, 0);
		setflags(s, s->A);
		break;
	case 6: //Bitwise OR
		s->A |= val;
		SET_FLAG(AC, 0);
		SET_FLAG(C, 0);
		setflags(s, s->A);
		break;
	case 7: //Compare
		SET_FLAG(AC, (s->A & 0xF) >= (val & 0xF));
		SET_FLAG(C, val > s->A);
		setflags(s, (uint8_t)(s->A - val));
		break;
	}
}

/*
// Instruction definitions
*/
static int op0(i8080_state *s, uint8_t op)
{
	int ret = cycles[op];
	uint8_t lowoct = (op & 007);
	uint8_t highoct = (op & 070) >> 3;

	switch (lowoct)
	{
	case 0: //NOP
		break;
	case 1: //16-bit add and immediate load (DAD LXI)
		if (highoct & 0x1) //DAD
		{
			uint32_t res = s->HL + reg16rd(s, (highoct & 0x06) >> 1);
			SET_FLAG(C, res > 0xFFFF); //Overflow
			s->HL = res & 0xFFFF;
		}
		else //LXI
			reg16wr(s, (highoct & 0x6) >> 1, im16(s));
		break;
	case 2: //Stores and Loads (STAX LDAX SHLD LHLD STA LDA)
		switch (highoct)
		{
		case 0: //STAX BC
			s->m_wr(s, s->BC, s->A);
			break;
		case 1: //LDAX BC
			s->A = s->m_rd(s, s->BC);
			break;
		case 2: //STAX DE
			s->m_wr(s, s->DE, s->A);
			break;
		case 3: //LDAX DE
			s->A = s->m_rd(s, s->DE);
			break;
		case 4: //SHLD
			m_wr16(s, im16(s), s->HL);
			break;
		case 5: //LHLD
			s->HL = m_rd16(s, im16(s));
			break;
		case 6: //STA
			s->m_wr(s, im16(s), s->A);
			break;
		case 7: //LDA
			s->A = s->m_rd(s, im16(s));
			break;
		}
		break;
	case 3: //16-bit inc/decrement (INX DCX)
	{
		uint16_t val = reg16rd(s, (highoct & 0x6) >> 1);
		val += (highoct & 0x1) ? -1 : 1;
		reg16wr(s, (highoct & 0x6) >> 1, val);
		break;
	}
	case 4: //INR
	{
		uint8_t val = reg8rd(s, highoct);
		SET_FLAG(AC, (val & 0xF) + 1 > 0xF)
		val++;
		reg8wr(s, highoct, val);
		setflags(s, val);
		break;
	}
	case 5: //DCR
	{
		uint8_t val = reg8rd(s, highoct);
		SET_FLAG(AC, (val & 0xF) >= 1);
		val--;
		reg8wr(s, highoct, val);
		setflags(s, val);
		break;
	}
	case 6: //MVI
		reg8wr(s, highoct, im8(s));
		break;
	case 7: //Rotates and misc arithmetic (RLC RRC RAL RAR DAA CMA STC CMC)
		switch (highoct)
		{
		case 0: //RLC
			SET_FLAG(C, s->A & 0x80);
			s->A = (s->A << 1) | (s->A >> 7);
			break;
		case 1: //RRC
			SET_FLAG(C, s->A & 0x01);
			s->A = (s->A >> 1) | (s->A << 7);
			break;
		case 2: //RAL
		{
			uint8_t carry = GET_FLAG(C);
			SET_FLAG(C, s->A & 0x80);
			s->A = (s->A << 1) | (carry);
			break;
		}
		case 3: //RAR
		{
			uint8_t carry = GET_FLAG(C);
			SET_FLAG(C, s->A & 0x01);
			s->A = (s->A >> 1) | (carry << 7);
			break;
		}
		case 4: //DAA
			if ((s->A > 0x99) || GET_FLAG(C)) //High nibble
			{
				SET_FLAG(C, 1);
				s->A += 0x60;
			}
			if (((s->A & 0xF) > 0x9) || GET_FLAG(AC)) //Low nibble
			{
				SET_FLAG(AC, ((s->A & 0xF) + 6) > 0xF);
				s->A += 6;
			}
			setflags(s, s->A);
			break;
		case 5: //CMA
			s->A = ~s->A;
			break;
		case 6: //STC
			SET_FLAG(C, 1);
			break;
		case 7: //CMC
			SET_FLAG(C, !GET_FLAG(C));
			break;
		}
		break;
	}
	return ret;
}

static int op1(i8080_state *s, uint8_t op)
{
	int ret = cycles[op];
	if (op == 0x76) //HLT
	{
		s->halted = 1;
		return ret;
	}
	else //MOV
	{
		uint8_t val = reg8rd(s, op & 007);
		reg8wr(s, (op & 070) >> 3, val);
		return ret;
	}
}

static int op2(i8080_state *s, uint8_t op)
{
	int ret = cycles[op];
	uint8_t lowoct = (op & 007); //Register
	uint8_t highoct = (op & 070) >> 3; //Operation

	uint8_t val = reg8rd(s, lowoct);
	alu(s, highoct, val);
	return ret;
}

static int op3(i8080_state *s, uint8_t op)
{
	int ret = cycles[op];
	uint8_t lowoct = (op & 007);
	uint8_t highoct = (op & 070) >> 3;

	switch (lowoct)
	{
	case 0: //Conditional return (RNZ RZ RNC RC RPO RPE RP RM)
		if (condition(s, highoct))
		{
			s->PC = stk_pop(s);
			ret += 6; //Cycles
		}
		break;
	case 1: //Stack pop, unconditional return and misc (RET POP PCHL SPHL)
		if (highoct & 0x1) //RET PCHL SPHL
		{
			switch ((highoct & 0x6) >> 1)
			{
			case 0: //RET
			case 1:
				s->PC = stk_pop(s);
				break;
			case 2: //PCHL
				s->PC = s->HL;
				break;
			case 3: //SPHL
				s->SP = s->HL;
				break;
			}
		}
		else //POP
		{
			uint16_t val = stk_pop(s);
			if (((highoct & 0x6) >> 1) == 0x3) //0b11 refers to PSW not SP!
				s->PSW = (val & 0xFFD7) | 0x2; //Set unused bits appropriately
			else
				reg16wr(s, (highoct & 0x6) >> 1, val);
		}
		break;
	case 2: //Conditional jump (JNZ JZ JNC JC JPO JPE JP JM)
	{
		uint16_t tgt = im16(s);
		if (condition(s, highoct))
			s->PC = tgt;
		break;
	}
	case 3: //Unconditional jump, IO, IRQ and misc (JMP OUT IN XTHL XCHG DI EI)
		switch (highoct)
		{
		case 0: //JMP
		case 1:
			s->PC = im16(s);
			break;
		case 2: //OUT
			s->io_wr(s, im8(s), s->A);
			break;
		case 3: //IN
			s->A = s->io_rd(s, im8(s));
			break;
		case 4: //XTHL
		{
			uint16_t temp = s->HL;
			s->HL = m_rd16(s, s->SP);
			m_wr16(s, s->SP, temp);
			break;
		}
		case 5: //XCHG
		{
			uint16_t temp = s->DE;
			s->DE = s->HL;
			s->HL = temp;
			break;
		}
		case 6: //DI
			s->irq_enabled = 0;
			break;
		case 7: //EI
			s->irq_enabled = 1;
			break;
		}
		break;
	case 4: //Conditonal call (CNZ CZ CNC CC CPO CPE CP CM)
	{
		uint16_t tgt = im16(s);
		if (condition(s, highoct))
		{
			stk_push(s, s->PC);
			s->PC = tgt;
			ret += 6; //Cycles
		}
		break;
	}
	case 5: //Stack push and call (PUSH CALL)
		if (highoct & 0x1) //CALL
		{
			uint16_t tgt = im16(s);
			stk_push(s, s->PC);
			s->PC = tgt;
		}
		else //PUSH
		{
			uint16_t val;
			if (((highoct & 0x6) >> 1) == 0x3) //0b11 refers to PSW not SP!
				val = s->PSW;
			else
				val = reg16rd(s, (highoct & 0x6) >> 1);
			stk_push(s, val);
		}
		break;
	case 6: //Intermediate arithmetic (ADI ACI SUI SBI ANI AXI ORI CPI)
		alu(s, highoct, im8(s));
		break;
	case 7: //RST
		stk_push(s, s->PC);
		s->PC = (highoct * 8);
		break;
	}
	return ret;
}

/*
// Public API
*/
void i8080_reset(i8080_state *s)
{
	//Registers
	s->PSW = 0;
	s->BC = 0;
	s->DE = 0;
	s->HL = 0;
	s->SP = 0;
	s->PC = 0;
	//State flags
	s->halted = 0;
	s->irq_enabled = 0;
	s->int_flag = 0;
	s->int_val = 0;
}

int i8080_cycle(i8080_state *s)
{
	uint8_t opcode = 0;

	if (s->irq_enabled && s->int_flag)
	{
		s->int_flag = 0;
		s->irq_enabled = 0;
		s->halted = 0;
		opcode = s->int_val;
	}
	else
	{
		if (s->halted)
			return 4;//Cycles
		opcode = s->m_rd(s, s->PC++);
	}

	switch ((opcode & 0300) >> 6)
	{
	default:
	case 0: return op0(s, opcode); //Misc
	case 1: return op1(s, opcode); //MOVs and HLT
	case 2: return op2(s, opcode); //ALU ops
	case 3: return op3(s, opcode); //Misc
	}
}

void i8080_int(i8080_state *s, uint8_t opcode)
{
	s->int_flag = 1;
	s->int_val = opcode;
}

#ifdef i8080_DEBUG
#include <stdio.h>
void i8080_printreg(i8080_state *s)
{
	printf("PC:%04X | ", s->PC);
	printf("OP:%02X | ", s->m_rd(s, s->PC));
	printf("A:%02X | ", s->A);
	printf("F:%02X | ", s->F);
	printf("BC:%04X | ", s->BC);
	printf("DE:%04X | ", s->DE);
	printf("HL:%04X | ", s->HL);
	printf("SP:%04X | ", s->SP);
	printf("Status:");
	putchar((s->F & (1 << 0)) ? 'C' : ' ');
	putchar((s->F & (1 << 2)) ? 'P' : ' ');
	putchar((s->F & (1 << 6)) ? 'Z' : ' ');
	putchar((s->F & (1 << 7)) ? 'S' : ' ');
	putchar((s->F & (1 << 4)) ? 'A' : ' ');
	printf("\n");
}
#endif
