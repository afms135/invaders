#ifndef I8080_H
#define I8080_H
#include <stdint.h>

//
//	Define a register pair
//
#define REGDEF(R16, R8H, R8L) \
	union \
	{ \
		struct \
		{ \
			uint8_t R8L; \
			uint8_t R8H; \
		}; \
		uint16_t R16; \
	} \

//
//	Compile time options
//
#define i8080_DEBUG //Enable i8080_printreg() (requires printf())
//#define i8080_BIGENDIAN //Define on big endian systems

//
//	Error codes
//
#define i8080_HLT -1

//
//	RST instructions
//
#define i8080_RST0 0xC7
#define i8080_RST1 0xCF
#define i8080_RST2 0xD7
#define i8080_RST3 0xDF
#define i8080_RST4 0xE7
#define i8080_RST5 0xEF
#define i8080_RST6 0xF7
#define i8080_RST7 0xFF

//
//	i8080 state
//
typedef struct i8080_state
{
	//Registers
#ifdef i8080_BIGENDIAN
	REGDEF(PSW, F, A);
	REGDEF(BC, C, B);
	REGDEF(DE, E, D);
	REGDEF(HL, L, H);
#else //Little endian
	REGDEF(PSW, A, F);
	REGDEF(BC, B, C);
	REGDEF(DE, D, E);
	REGDEF(HL, H, L);
#endif
	uint16_t SP;
	uint16_t PC;

	//State flags
	uint8_t halted;
	uint8_t irq_enabled;
	uint8_t int_flag;
	uint8_t int_val;

	//Read/Write callbacks
	uint8_t(*m_rd)(struct i8080_state *state, uint16_t addr);
	void(*m_wr)(struct i8080_state *state, uint16_t addr, uint8_t val);
	uint8_t(*io_rd)(struct i8080_state *state, uint8_t port);
	void(*io_wr)(struct i8080_state *state, uint8_t port, uint8_t val);
} i8080_state;

//
//	API
//
//Reset i8080 CPU
void i8080_reset(i8080_state *state);

//Perform one CPU cycle
int i8080_cycle(i8080_state *s);

//Signal an interrupt
void i8080_int(i8080_state *s, uint8_t opcode);

//Print i8080 register states
#ifdef i8080_DEBUG
void i8080_printreg(i8080_state *s);
#endif

#endif /*I8080_H*/
