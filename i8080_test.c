#include "i8080.h"
#include <stdio.h>

int running = 1;
uint8_t mem[65536];

uint8_t mem_rd(i8080_state *s, uint16_t addr)
{
	if (addr == 0) //CP/M Exit
		running = 0;
	else if (addr == 5) //BDOS
	{
		if (s->C == 9) //Print $ terminated string
		{
			uint16_t addr = s->DE;
			while (s->m_rd(s, addr) != '$')
			{
				putchar(s->m_rd(s, addr));
				addr++;
			}
		}
		else if (s->C == 2) //Putchar
			putchar(s->E);
		return 0xC9; //RET
	}

	return mem[addr];
}

void mem_wr(i8080_state *s, uint16_t addr, uint8_t val)
{
	(void)s; //Surpress warning
	mem[addr] = val;
}

uint8_t io_rd(i8080_state *s, uint8_t port)
{
	(void)s;
	(void)port; //Surpress warning
	return 0;
}

void io_wr(i8080_state *s, uint8_t port, uint8_t v)
{
	(void)s;
	(void)port;
	(void)v;
}

int runtest(i8080_state *s, char *file, int n)
{
	printf("Test %u: %s\n", n, file);
	printf("----------------------------------------\n");

	FILE *f = fopen(file, "rb");
	if (f == NULL)
		return -1; //Could not open file
	fread(mem + 0x100, 1, sizeof(mem) - 0x100, f);
	fclose(f);

	i8080_reset(s);
	s->PC = 0x100;
	running = 1;
	while (running)
		i8080_cycle(s);
	printf("\n----------------------------------------\n\n\n");
	return 0;
}

int main(void)
{
	i8080_state s;

	s.m_rd = mem_rd;
	s.m_wr = mem_wr;
	s.io_rd = io_rd;
	s.io_wr = io_wr;

	printf("Cycle table\n");
	for (int i = 0; i < 256; i++)
	{
		if (i != 0 && (i % 16 == 0))
			printf("\n");
		i8080_reset(&s);
		s.PC = 0x100;
		mem[0x100] = i;
		printf("%02u ", i8080_cycle(&s));
	}
	printf("\n\n");

#define NUM_TESTS 4
	char *tests[NUM_TESTS] =
	{
		"tests/TEST.COM",
		"tests/CPUTEST.COM",
		"tests/8080PRE.COM",
		"tests/8080EX1.COM"
	};

	for (int i = 0; i < NUM_TESTS; i++)
	{
		int ret = runtest(&s, tests[i], i);
		if (ret)
			return ret; //Error occured, exit
	}

	return 0;
}
