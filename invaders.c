#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <errno.h>
#ifdef WIN32
#include <SDL.h>
#else
#include "icon.h"
#include <SDL2/SDL.h>
#endif
#include "i8080.h"

//
//	Defines
//
#define CYCLES_PER_FRAME (2000000UL/60)
#define DIP1 1 //Lives (DIP2,DIP1) 00-3 01-4
#define DIP2 0 //Lives (DIP2,DIP1) 10-5 11-6
#define DIP3 0 //Power on test (Not used on all ROM sets)
#define DIP4 1 //Bonus life at 0=1000 1=1500
#define DIP8 1 //Coin info in attract mode (0-true 1-false)
//#define ROM_HEADER //Define if ROM.h exists

//
//	Memory map
//
#ifndef ROM_HEADER
uint8_t ROM[8192]; //4x 2k ROMS
#else
#include "ROM.h"
#endif
uint8_t RAM[1024]; //   1k Work RAM
uint8_t VRAM[7168];//   7k Video RAM

//
//	Shift register
//
uint16_t shift_reg = 0;
uint8_t shift_off = 0;

//
//	Function prototypes
//
static uint8_t mem_rd(i8080_state *s, uint16_t addr);
static void mem_wr(i8080_state *s, uint16_t addr, uint8_t val);
static uint8_t io_rd(i8080_state *s, uint8_t port);
static void io_wr(i8080_state *s, uint8_t port, uint8_t val);
static uint8_t get_colour(uint8_t row, uint8_t column);
static void VRAM_render(SDL_Texture *tex, int colour);
static char *string_concat(size_t n, ...);
static char *hiscore_path(void);
#ifndef ROM_HEADER
static int load_ROM(const char *file, size_t off, SDL_Window *win);
#endif
static void load_hiscore(uint8_t *msb, uint8_t *lsb);
static void save_hiscore(uint8_t msb, uint8_t lsb);

//
//	Main
//
int main(void)
{
	//
	//	Init SDL
	//
	SDL_Window *win = NULL;
	SDL_Renderer *rend = NULL;
	SDL_Texture *tex = NULL;

	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		fprintf(stderr, "Error: SDL_Init() %s\n", SDL_GetError());
		goto ERR;
	}

	win = SDL_CreateWindow("Space Invaders", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 224, 256, 0);
	if (win == NULL)
	{
		SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "SDL_CreateWindow()", SDL_GetError(), NULL);
		goto ERR;
	}

	rend = SDL_CreateRenderer(win, -1, 0);
	if (rend == NULL)
	{
		SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "SDL_CreateRenderer()", SDL_GetError(), win);
		goto ERR;
	}

	tex = SDL_CreateTexture(rend, SDL_PIXELFORMAT_RGB332, SDL_TEXTUREACCESS_STREAMING, 224, 256);
	if (tex == NULL)
	{
		SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "SDL_CreateTexture()", SDL_GetError(), win);
		goto ERR;
	}

	if (!SDL_RenderSetLogicalSize(rend, 224, 256) && !SDL_RenderSetIntegerScale(rend, SDL_TRUE))
	{
		SDL_SetWindowMinimumSize(win, 224, 256);
		SDL_SetWindowResizable(win, SDL_TRUE);
	}

	//
	//	Load application icon
	//
#ifdef WIN32
	SDL_SetHint(SDL_HINT_WINDOWS_INTRESOURCE_ICON, "101");
	SDL_SetHint(SDL_HINT_WINDOWS_INTRESOURCE_ICON_SMALL, "101");
#else
	SDL_RWops *ops = SDL_RWFromConstMem(icon_bmp, icon_bmp_len);
	if (ops != NULL)
	{
		SDL_Surface *icon = SDL_LoadBMP_RW(ops, 1);
		if (icon != NULL)
			SDL_SetWindowIcon(win, icon);
		SDL_FreeSurface(icon);
	}
#endif

#ifndef ROM_HEADER
	//
	//	Load ROMS
	//
	if (load_ROM("invaders.h", 0x0000, win)) //invaders.h 0000 - 07FF
		goto ERR;
	if (load_ROM("invaders.g", 0x0800, win)) //invaders.g 0800 - 0FFF
		goto ERR;
	if (load_ROM("invaders.f", 0x1000, win)) //invaders.f 1000 - 17FF
		goto ERR;
	if (load_ROM("invaders.e", 0x1800, win)) //invaders.e 1800 - 1FFF
		goto ERR;
#endif

	//
	//	Init i8080 emulator
	//
	i8080_state s;
	s.m_rd = mem_rd;
	s.m_wr = mem_wr;
	s.io_rd = io_rd;
	s.io_wr = io_wr;

	i8080_reset(&s);

	//
	//	Load hi-score
	//
	//Load sentinel value
	RAM[0xF4] = 0xAB; //Hi-Score LSB BCD
	RAM[0xF5] = 0xCD; //Hi-score MSB BCD
	//Wait for game to zero score
	while (RAM[0xF4] != 0x00 || RAM[0xF5] != 0x00)
		i8080_cycle(&s);
	//Load hi-score
	load_hiscore(&RAM[0xF5], &RAM[0xF4]);

	//
	//	Main loop
	//

	int colour = 1;
	int running = 1;
	Uint32 t1, t2;
	int cycles = 0;
	while (running)
	{
		t1 = SDL_GetTicks();
		SDL_SetRenderDrawColor(rend, 0, 0, 0, 255);
		SDL_RenderClear(rend);

		SDL_Event event;
		while (SDL_PollEvent(&event))
		{
			if (event.type == SDL_QUIT)
				running = 0;
			else if (event.type == SDL_KEYDOWN)
			{
				if (event.key.keysym.sym == SDLK_g)
					colour = !colour;
				else if (event.key.keysym.sym == SDLK_r)
					i8080_reset(&s);
			}
		}

		//Execute
		cycles += CYCLES_PER_FRAME / 2;
		while (cycles > 4)
			cycles -= i8080_cycle(&s);

		//Wait until mid frame
		t2 = SDL_GetTicks();
		if (t2 - t1 < 8)
			SDL_Delay(8 - (t2 - t1));

		//Mid-screen interrupt
		i8080_int(&s, i8080_RST1);

		//Execute
		t1 = SDL_GetTicks();
		cycles += CYCLES_PER_FRAME / 2;
		while (cycles > 4)
			cycles -= i8080_cycle(&s);

		//Wait until end frame
		t2 = SDL_GetTicks();
		if (t2 - t1 < 8)
			SDL_Delay(8 - (t2 - t1));

		//V-Blank interrupt
		i8080_int(&s, i8080_RST2);

		//Render
		VRAM_render(tex, colour);
		SDL_RenderCopy(rend, tex, NULL, NULL);
		SDL_RenderPresent(rend);
	}

	//
	//	Save hi-score
	//
	save_hiscore(RAM[0xF5], RAM[0xF4]);

	//
	//	Cleanup
	//
	SDL_DestroyTexture(tex);
	SDL_DestroyRenderer(rend);
	SDL_DestroyWindow(win);
	SDL_Quit();
	return 0;

ERR:if (tex != NULL)  SDL_DestroyTexture(tex);
	if (rend != NULL) SDL_DestroyRenderer(rend);
	if (win != NULL)  SDL_DestroyWindow(win);
	SDL_Quit();
	return -1;
}

//
//	i8080 Callbacks
//
static uint8_t mem_rd(i8080_state *s, uint16_t addr)
{
	if (addr <= 0x1FFF) //ROM
		return ROM[addr];
	else if (addr >= 0x2000 && addr <= 0x23FF) //Work RAM
		return RAM[addr - 0x2000];
	else if (addr >= 0x2400 && addr <= 0x3FFF) //Video RAM
		return VRAM[addr - 0x2400];
	else if (addr >= 0x4000 && addr <= 0x43FF) //Work RAM Mirror
		return RAM[addr - 0x4000];
	else if (addr >= 0x4400 && addr <= 0x5FFF) //Video RAM Mirror
		return VRAM[addr - 0x4400];
	else
		fprintf(stderr, "Invalid read from address %04X PC=%04X\n", addr, s->PC);
	return 0x00; //NOP
}

static void mem_wr(i8080_state *s, uint16_t addr, uint8_t val)
{
	if (addr >= 0x2000 && addr <= 0x23FF) //Work RAM
		RAM[addr - 0x2000] = val;
	else if (addr >= 0x2400 && addr <= 0x3FFF) //Video RAM
		VRAM[addr - 0x2400] = val;
	else if (addr >= 0x4000 && addr <= 0x43FF) //Work RAM Mirror
		RAM[addr - 0x4000] = val;
	else if (addr >= 0x4400 && addr <= 0x5FFF) //Video RAM Mirror
		VRAM[addr - 0x4400] = val;
	else
		fprintf(stderr, "Invalid write %02X to address %04X PC=%04X\n", val, addr, s->PC);
}

static uint8_t io_rd(i8080_state *s, uint8_t port)
{
	const Uint8 *keys = SDL_GetKeyboardState(NULL);
	//Convert keys to scancodes
	uint8_t coin = keys[SDL_GetScancodeFromKey(SDLK_c)];
	uint8_t tilt = keys[SDL_GetScancodeFromKey(SDLK_t)];
	uint8_t p1   = keys[SDL_GetScancodeFromKey(SDLK_1)];
	uint8_t p2   = keys[SDL_GetScancodeFromKey(SDLK_2)];

	switch (port)
	{
	case 0: //Input
		return
			(DIP3 << 0) | //DIP3 (Power-on test)
			(0 << 1) | //Unused
			(0 << 2) | //Unused
			(0 << 3) | //Unused
			(0 << 4) | //Fire  (Unused)
			(0 << 5) | //Left  (Unused)
			(0 << 6) | //Right (Unused)
			(0 << 7);  //Unused
	case 1: //Input
		return
			(coin << 0) | //Coin
			(p2 << 1) | //2P Start
			((keys[SDL_SCANCODE_RETURN] || p1) << 2) | //1P Start
			(0 << 3) | //Unused
			(keys[SDL_SCANCODE_UP] << 4) | //1P Shoot
			(keys[SDL_SCANCODE_LEFT] << 5) | //1P Left
			(keys[SDL_SCANCODE_RIGHT] << 6) | //1P Right
			(0 << 7);  //Unused
	case 2: //Input
		return
			(DIP1 << 0) | //DIP1 (Lives)
			(DIP2 << 1) | //DIP2 (Lives)
			(tilt << 2) | //Tilt
			(DIP4 << 3) | //DIP4 (Bonus life)
			(keys[SDL_SCANCODE_W] << 4) | //P2 Shoot
			(keys[SDL_SCANCODE_A] << 5) | //P2 Left
			(keys[SDL_SCANCODE_D] << 6) | //P2 Right
			(DIP8 << 7);  //DIP8 (Coin info)
	case 3: //Shift register data
		return shift_reg >> (8 - shift_off);
	default:
		fprintf(stderr, "Invalid I/O read from port %02X PC=%04X\n", port, s->PC);
	}
	return 0x00;
}

static void io_wr(i8080_state *s, uint8_t port, uint8_t val)
{
	switch (port)
	{
	case 2: //Shift register offset
		shift_off = val & 0x7; //Bits 0,1,2
		return;
	case 3: //Sound output (Not implemented)
		return;
	case 4: //Shift register input
		shift_reg = (val << 8) | (shift_reg >> 8);
		return;
	case 5: //Sound output (Not implemented)
		return;
	case 6: //Watchdog (Not used)
		return;
	default:
		fprintf(stderr, "Invalid I/O write %02X to port %02X PC=%04X\n", val, port, s->PC);
		return;
	}
}

//
//	Rendering
//
static uint8_t get_colour(uint8_t row, uint8_t column)
{
	if (row < 32)
		return 0xFF; //White
	else if (row >= 32 && row < 64)
		return 0xE0; //Red
	else if (row >= 64 && row < 184)
		return 0xFF; //White
	else if (row >= 184 && row < 240)
		return 0x1C; //Green
	else
	{
		if (column < 16)
			return 0xFF; //White
		else if (column >= 16 && column < 134)
			return 0x1C; //Green
		else
			return 0xFF; //White
	}
	return 0x00; //Black
}

static void VRAM_render(SDL_Texture *tex, int colour)
{
	void *pixels;
	int pitch;

	SDL_LockTexture(tex, NULL, &pixels, &pitch);
	uint8_t (*pix)[224] = pixels; //pix[256][224] [Y][X]

	for (int col = 0; col < 224; col++) //Columns (left-to-right)
	{
		for (int row = 0; row < 256 / 8; row++) //Rows (byte packed) (bottom-to-top)
		{
			uint8_t byte = VRAM[(col * 32) + row];

			for (int i = 0; i < 8; i++)
			{
				int flip_row = 255 - ((row * 8) + i);
				uint8_t c = (colour) ? get_colour(flip_row, col) : 0xFF;
				pix[flip_row][col] = (byte & (1 << i)) ? c : 0;
			}
		}
	}

	SDL_UnlockTexture(tex);
}

//
//	File I/O Helpers
//
static char *string_concat(size_t n, ...)
{
	if (n < 1)
		return NULL;

	size_t len = 1; //NULL terminator
	char *ret = malloc(len);
	if (ret == NULL)
		return NULL;
	strcpy(ret, ""); //Empty

	va_list args;
	va_start(args, n);

	while (n)
	{
		const char *str = va_arg(args, const char*);
		len += strlen(str);

		void *buf = realloc(ret, len);
		if (buf == NULL)
		{
			free(ret);
			va_end(args);
			return NULL;
		}
		ret = buf;

		strcat(ret, str);
		n--;
	}

	va_end(args);
	return ret;
}

static char *hiscore_path(void)
{
	const char *file_path = "score.bin";
	char *folder_path = SDL_GetPrefPath("afms135", "Invaders");
	if (folder_path == NULL)
		goto ERR;

	char *path = string_concat(2, folder_path, file_path);
	if (path == NULL)
		goto ERR;

	SDL_free(folder_path);
	return path;

ERR:if (folder_path != NULL) SDL_free(folder_path);
	return NULL;
}

//
//	File I/O
//
#ifndef ROM_HEADER
static int load_ROM(const char *file, size_t off, SDL_Window *win)
{
	char *msg = "";

	FILE *f = fopen(file, "rb");
	if (f == NULL)
	{
		msg = string_concat(4, "Error opening ", file, ": ", strerror(errno));
		goto ERR;
	}

	size_t read = fread(ROM + off, 1, 2048, f);
	if (ferror(f))
	{
		msg = string_concat(4, "Error reading ", file, ": ", strerror(errno));
		goto ERRCLOSE;
	}
	if (read != 2048)
	{
		msg = string_concat(3, "Invalid ROM size", ": ", file);
		goto ERRCLOSE;
	}

	if (fclose(f) == EOF)
	{
		msg = string_concat(4, "Error closing ", file, ": ", strerror(errno));
		goto ERR;
	}
	return 0;

ERRCLOSE:
	fclose(f);
ERR:
	SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Space Invaders", msg, win);
	free(msg);
	return -1;
}
#endif

static void load_hiscore(uint8_t *msb, uint8_t *lsb)
{
	char *path = hiscore_path();
	if (path == NULL)
	{
		fprintf(stderr, "Error: Could not find save path\n");
		return;
	}

	FILE *f = fopen(path, "rb");
	if (f == NULL)
	{
		fprintf(stderr, "Error: Could not open file %s: %s\n", path, strerror(errno));
		goto ERR;
	}

	size_t ret = 0;
	uint8_t high, low;
	ret += fread(&high, 1, 1, f);
	ret += fread(&low, 1, 1, f);
	if (ret != 2)
	{
		fprintf(stderr, "Error: Could not read file %s: %s\n", path, strerror(errno));
		goto ERRCLOSE;
	}
	*msb = high;
	*lsb = low;

ERRCLOSE:
	if (fclose(f) == EOF)
		fprintf(stderr, "Error: Could not close file %s: %s\n", path, strerror(errno));
ERR:
	free(path);
}

static void save_hiscore(uint8_t msb, uint8_t lsb)
{
	char *path = hiscore_path();
	if (path == NULL)
	{
		fprintf(stderr, "Error: Could not find save path\n");
		return;
	}

	FILE *f = fopen(path, "wb");
	if (f == NULL)
	{
		fprintf(stderr, "Error: Could not open file %s: %s\n", path, strerror(errno));
		goto ERR;
	}

	size_t ret = 0;
	ret += fwrite(&msb, 1, 1, f);
	ret += fwrite(&lsb, 1, 1, f);
	if (ret != 2)
		fprintf(stderr, "Error: Could not write file %s: %s\n", path, strerror(errno));

	if (fclose(f) == EOF)
		fprintf(stderr, "Error: Could not close file %s: %s\n", path, strerror(errno));
ERR:
	free(path);
}
