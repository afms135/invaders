# Space Invaders Emulator

![Screenshot](screenshot.png)

This project is an emulator for the original Space Invaders arcade cabinet.
Requires SDL2.

Requires an unextracted `invaders.zip` ROM set with the following files:
- `invaders.e`
- `invaders.f`
- `invaders.g`
- `invaders.h`

## Controls

|     Key     |       Control      |
|-------------|--------------------|
| c           | Add credit         |
| Return / 1  | Start game (1P)    |
| 2           | Start game (2P)    |
| Left arrow  | Move left          |
| Right arrow | Move right         |
| Up arrow    | Shoot              |
| a           | Move left (P2)     |
| d           | Move right (P2)    |
| w           | Shoot (P2)         |
| r           | Reset              |
| g           | Turn on/off colour |
| t           | Tilt               |

## ROM Build

If desired the ROM set can be compiled into the binary.

To do this run `gen_romh.sh` before compiling and un-comment
`#define ROM_HEADER` at the top of `invaders.c`.

## DIP Switches

The arcade machine configuration can be set the top of `invaders.c`:

- `DIP1` and `DIP2` determine how many lives the player has at the start of the
game (Table below).

- `DIP3` enables the power on self test (this is not used in all versions of the
ROM).

- `DIP4` determines whether a bonus life is awarded at either 1000 or 1500
points.

- `DIP5` detemines whether the current number of credits is shown at the bottom
of the screen during attract mode

| DIP2 | DIP1 | Lives |
|------|------|-------|
|  0   |  0   |   3   |
|  0   |  1   |   4   |
|  1   |  0   |   5   |
|  1   |  1   |   6   |

## Compile

To compile run:

`$ gcc i8080.c invaders.c -lSDL2`

To compile the 8080 CPU core tests run:

`$ gcc i8080.c i8008_test.c`
